CFLAGS = -Wall -I../../../include

all: amp.so

amp.so: amp.o
	$(CXX) -o amp.so amp.o -shared

clean:
	rm -f *.o amp.so
