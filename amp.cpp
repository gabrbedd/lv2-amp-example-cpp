#include <stdlib.h>
#include <string.h>

#include <math.h>

#include "lv2.h"

#define AMP_URI       "http://lv2plug.in/plugins/example_amp";
#define AMP_GAIN      0
#define AMP_INPUT     1
#define AMP_OUTPUT    2

static LV2_Descriptor *ampDescriptor = NULL;

class Amp {
public:
	static void init();

private:
	float *m_gain;
	float *m_input;
	float *m_output;

	Amp() : m_gain(0), m_input(0), m_output(0) {
	}
	~Amp() {
	}

	void run_(uint32_t sample_count);
	void connectPort_(uint32_t port, void *data);

	static void cleanup(LV2_Handle instance);
	static void connectPort(LV2_Handle instance, uint32_t port,
				void *data);
	static LV2_Handle instantiate(const LV2_Descriptor *descriptor,
				      double s_rate, const char *path,
				      const LV2_Feature * const* features);
	static void run(LV2_Handle instance, uint32_t sample_count);
};

void Amp::cleanup(LV2_Handle instance) {
	delete (Amp*)(instance);
}

void Amp::connectPort(LV2_Handle instance, uint32_t port,
		      void *data)
{
	((Amp*)(instance))->connectPort_(port, data);
}	

void Amp::connectPort_(uint32_t port, void *data)
{
	switch (port) {
	case AMP_GAIN:
		m_gain = (float*)data;
		break;
	case AMP_INPUT:
		m_input = (float*)data;
		break;
	case AMP_OUTPUT:
		m_output = (float*)data;
		break;
	}
}

LV2_Handle Amp::instantiate(const LV2_Descriptor *descriptor,
			    double s_rate, const char *path,
			    const LV2_Feature * const* features)
{
	Amp* that = new Amp;

	return (LV2_Handle)that;
}

#define DB_CO(g) ((g) > -90.0f ? powf(10.0f, (g) * 0.05f) : 0.0f)

void Amp::run(LV2_Handle instance, uint32_t sample_count)
{
	((Amp*)(instance))->run_(sample_count);
}

void Amp::run_(uint32_t sample_count)
{
	int pos;
	float coef = DB_CO(*m_gain);

	for (pos = 0; pos < sample_count; ++pos) {
		m_output[pos] = m_input[pos] * coef;
	}
}

void Amp::init()
{
	ampDescriptor = new LV2_Descriptor;

	ampDescriptor->URI = AMP_URI;
	ampDescriptor->activate = NULL;
	ampDescriptor->cleanup = Amp::cleanup;
	ampDescriptor->connect_port = Amp::connectPort;
	ampDescriptor->deactivate = NULL;
	ampDescriptor->instantiate = Amp::instantiate;
	ampDescriptor->run = Amp::run;
	ampDescriptor->extension_data = NULL;
}

LV2_SYMBOL_EXPORT
const LV2_Descriptor *lv2_descriptor(uint32_t index)
{
	if (!ampDescriptor) Amp::init();

	switch (index) {
	case 0:
		return ampDescriptor;
	default:
		return NULL;
	}
}
